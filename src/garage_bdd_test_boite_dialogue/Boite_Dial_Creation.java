package garage_bdd_test_boite_dialogue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


import garage_bdd_test_DAO.MoteurDAO;
import garage_bdd_test_DAO.OptionDAO;
import garage_bdd_test_DAO.TypeMoteurDAO;
import garage_bdd_test_DAO.VehiculeDAO;
/**
 * Boite de dialogue modale pour la creation d'un vehicule
 * @author nicolas
 *
 */
public class Boite_Dial_Creation extends Boite_dial_modale {
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Constructeur avec param
	 * @param parent
	 * @param ptitre
	 * @param modal
	 */
	public Boite_Dial_Creation(JFrame parent, String ptitre, boolean modal) {
		super(parent, ptitre, modal);
		titre = "CREATION DE VEHICULE";
		
	}
	
	/**
	 * Methode initialisant les diff�rents composants de la boite de dialogue
	 */
	public void initComponent() {
		
		
		//--nom du vehicule
		JPanel panNom = new JPanel();
		panNom.setBackground(Color.WHITE);
		panNom.setPreferredSize(new Dimension (220,60));
		nom = new JTextField();
		nom.setPreferredSize(new Dimension(100,25));
		panNom.setBorder(BorderFactory.createTitledBorder("Nom du v�hicule"));
		JLabel nomLabel = new JLabel("Saisir un nom : ");
		panNom.add(nomLabel);
		panNom.add(nom);
		
		//--Marque
		JPanel panMarque = new JPanel();
		panMarque.setBackground(Color.WHITE);
		panMarque.setPreferredSize(new Dimension(220,60));
		panMarque.setBorder(BorderFactory.createTitledBorder("Marque du vehicule"));
		marque = new JComboBox();
		marque.addItem("PIGEOT");
		marque.addItem("TROEN");
		marque.addItem("RENO");
		JLabel marqueLabel = new JLabel("Marque : ");
		panMarque.add(marqueLabel);
		panMarque.add(marque);
		
		//--Image du vehicule selon la marque
		String constrVehic = (String)marque.getSelectedItem();
		switch(constrVehic) {
			case("RENO") : this.icon = new JLabel(new ImageIcon("Ressource/Images/RENO.JPG")); break;
			case("TROEN") : this.icon = new JLabel(new ImageIcon("Ressource/Images/TROEN.JPG")) ; break;
			case ("PIGEOT") : this.icon = new JLabel(new ImageIcon("Ressource/Images/PIGEOT.JPG")); break;
		}
				
		//--type de moteur
		JPanel panTypeMoteur = new JPanel();
		panTypeMoteur.setBackground(Color.WHITE);
		panTypeMoteur.setPreferredSize(new Dimension(440,60));
		panTypeMoteur.setBorder(BorderFactory.createTitledBorder("Type du moteur du vehicule"));
		JRadioButton tranche1 = new JRadioButton("ESSENCE");
		tranche1.setSelected(true);
		JRadioButton tranche2 = new JRadioButton("DIESEL");
		JRadioButton tranche3 = new JRadioButton("HYBRIDE");
		JRadioButton tranche4 = new JRadioButton("ELECTRIQUE");
		ButtonGroup bg = new ButtonGroup();
		bg.add(tranche1);
		bg.add(tranche2);
		bg.add(tranche3);
		bg.add(tranche4);
		panTypeMoteur.add(tranche1);
		panTypeMoteur.add(tranche2);
		panTypeMoteur.add(tranche3);
		panTypeMoteur.add(tranche4);
		this.typeMoteur = "";
		if(tranche1.isSelected())
			typeMoteur = tranche1.getText();
		else if (tranche2.isSelected())
			typeMoteur = tranche2.getText();
		else if (tranche3.isSelected())
			typeMoteur = tranche3.getText();
		else if (tranche4.isSelected())
			typeMoteur = tranche4.getText();
		
		//--cylindree du vehicule
		JPanel panCyl = new JPanel();
		panCyl.setBackground(Color.WHITE);
		panCyl.setPreferredSize(new Dimension(220,60));
		panCyl.setBorder(BorderFactory.createTitledBorder("Cylindree du vehicule"));
		cylindre = new JComboBox();
		switch(typeMoteur) {
			case("ESSENCE") : cylindre.addItem("150 chevaux");
							  cylindre.addItem("200 chevaux");
							  break;
			case("DIESEL") : cylindre.addItem("150 Hdi");
							 cylindre.addItem("180 Hdi");
							 cylindre.addItem("250 Hdi");
							 break;
			case("HYBRIDE") : cylindre.addItem("Essence/Nucleaire");
							  cylindre.addItem("Essence/Eolienne");
							  break;
			case("ELECTRIQUE") : cylindre.addItem("100 Kw");
								 cylindre.addItem("1000 Kw");
								 break;
		}
		JLabel cylindreLabel = new JLabel("Cylindree : ");
		panCyl.add(cylindreLabel);
		panCyl.add(cylindre);
		
		//--Options
		JPanel panOption = new JPanel();
		panOption.setBackground(Color.WHITE);
		panOption.setPreferredSize(new Dimension(440,60));
		panOption.setBorder(BorderFactory.createTitledBorder("Options du vehicule"));
		option1 = new JCheckBox("Toit ouvrant");
		option2 = new JCheckBox("Climatisation");
		option3 = new JCheckBox("GPS");
		option4 = new JCheckBox("Sieges chauffants");
		option5 = new JCheckBox("Barres de toit");
		panOption.add(option1);
		panOption.add(option2);
		panOption.add(option3);
		panOption.add(option4);
		panOption.add(option5);
		
		//--Prix du vehicule
		JPanel panPrix = new JPanel();
		panPrix.setBackground(Color.WHITE);
		panPrix.setPreferredSize(new Dimension (220,60));
		panPrix.setBorder(BorderFactory.createTitledBorder("Prix du vehicule"));
		JLabel prixLabel1 = new JLabel("Prix");
		JLabel prixLabel2 = new JLabel(" �");
		prix = new JFormattedTextField(NumberFormat.getNumberInstance());
		prix.setPreferredSize(new Dimension (90,25));
		
		//double prixCalc = prixSaisi + ; ici j'additionne le prix saisi au prix des options et du moteur 
		panPrix.add(prixLabel1);
		panPrix.add(prix);
		panPrix.add(prixLabel2);
				
		//--On ajoute les composants precedents au JPanel qui doit recevoir tout cela		
		panContent.add(panNom);
		panContent.add(panMarque);
		panContent.add(panTypeMoteur);
		panContent.add(panCyl);
		panContent.add(panOption);
		panContent.add(panPrix);
		
		//--On ajoute un listener a notre bouton OK
		this.okBouton.addActionListener(new BoutonListener());
	}
	/**
	 * classe interne qui determine le comportement du bouton OK
	 * @author nicolas
	 *
	 */
	class BoutonListener implements ActionListener{
		/**
		 * Methode qui utilise les donnees entrees par l'utilisateur pour creer un vehicule dans la bdd
		 */
		public void actionPerformed(ActionEvent e) {
			VehiculeDAO vehicDAO = new VehiculeDAO(conn);
			Vehicule vehiculeCree = new Vehicule(nom.getText(), (String)marque.getText(), getMoteur(), getOption(), getPrix());
			vehicDAO.create(vehiculeCree);
			setVisible(false);
		}
		
		/**
		 * Methode qui renvoie l'objet moteur du vehicule cree
		 * @return Moteur
		 */
		public Moteur getMoteur() {
			MoteurDAO moteurDAO = new MoteurDAO(conn); 
			Moteur moteurCree = new Moteur(moteurDAO.getIdentifiant((String)cylindre.getText()));
			return moteurCree;
		}
		
		/**
		 * Methode qui renvoie le type de moteur du vehicule cree. Inutile
		 * @return
		 */
		public TypeMoteur getTypeMoteur() {
			TypeMoteurDAO tmDAO = new TypeMoteurDAO(conn);
			TypeMoteur typeMotCree = new TypeMoteur (tmDAO.getIdentifiant(typeMoteur));
			return typeMotCree ;
		}
		
		/**
		 * Methode qui renvoie la liste des options du vehicule cree
		 * @return
		 */
		public List<Option> getOption(){
			OptionDAO optDAO = new OptionDAO(conn);
			List<Option> optionCree = new List<Option>();
			if (option1.isSelected()) {
				Option opt1 = new Option(optDAO.getIdentifiant((String)option1.getSelectedItem()));
				optionCree.add(opt1);
			}
			else if (option2.isSelected()) {
				Option opt2 = new Option(optDAO.getIdentifiant((String)option2.getSelectedItem()));
				optionCree.add(opt2);
			}	
			else if (option3.isSelected()) {
				Option opt3 = new Option(optDAO.getIdentifiant((String)option3.getSelectedItem()));
				optionCree.add(opt3);
			}
			else if (option4.isSelected()) {
				Option opt4 = new Option(optDAO.getIdentifiant((String)option4.getSelectedItem()));
				optionCree.add(opt4);
			}
			else if (option5.isSelected()) {
				Option opt5 = new Option(optDAO.getIdentifiant((String)option5.getSelectedItem()));
				optionCree.add(opt5);
			}
			return optionCree;
		}
		/**
		 * Methode qui renvoie le prix total, somme des prix saisis, des prix des options et du prix du moteur
		 * @return
		 */
		public double getPrix() {
			double prixMotCree = this.getMoteur().getPrix();
			double prixOptCree = 0;
			for (Option opt : this.getOption())
				prixOptCree += opt.getPrix();
			prixTot = (((Number) prix.getValue()).doubleValue() + prixMotCree + prixOptCree;
			return prixTot;			
		}
	}
}
