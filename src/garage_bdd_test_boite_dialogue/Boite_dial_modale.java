package garage_bdd_test_boite_dialogue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Classe abstraite boite de dialogue modale dont herite les 3 boites de dialogue : creation, detail, suppression
 * @author nicolas
 *
 */
public abstract class Boite_dial_modale extends JDialog {
	private static final long serialVersionUID = 1L;
	protected Connection conn = HsqldbConnection.getInstance();
	protected JPanel panContent = new JPanel();
	protected JLabel icon = new JLabel(new ImageIcon());
	protected JButton okBouton = new JButton("OK");
	protected JTextField nom;
	protected JComboBox<String> marque, cylindre;
	protected JFormattedTextField prix;
	protected JCheckBox option1, option2, option3, option4, option5;
	protected String typeMoteur, titre;
	protected double prixTot;
	
	/**
	 * Constructeur avec parametre
	 * @param parent
	 * @param ptitre
	 * @param modal
	 */
	public Boite_dial_modale (JFrame parent, String ptitre, boolean modal) {
		super(parent, ptitre, modal);
		this.setSize(550, 270);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
		
		//--on init la co a la base de donnee
		this.conn.setAutoCommit(false);
		
		//--On appel la methode qui initialise les différents panneaux de donnees
		this.initComponent();
		//--Le panneau qui recueille les composants de la methodes precedente.Vide pour l'instant
		panContent.setLayout(new FlowLayout());
		panContent.setBackground(Color.WHITE);
		
		//--Le panneau qui recueille les boutons
		JPanel panBouton = new JPanel();
		panBouton.setLayout(new FlowLayout());
		//--Le bouton OK est juste creer, son comportement est def ds les classes filles
		//--Bouton ANNULER
		JButton annulBouton = new JButton ("ANNULER");
		annulBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		panBouton.add(okBouton);
		panBouton.add(annulBouton);
		
		//--Le panneau qui accueuille l'image
		JPanel panIcon = new JPanel();
		panIcon.setBackground(Color.WHITE);
		panIcon.setLayout(new BorderLayout());
		panIcon.add(icon);
		
		//--Une etiquette de titre
		JLabel titreLbl = new JLabel();
		Font policetitre = new Font("Arial", Font.BOLD, 25);
		titreLbl.setFont(policetitre);
		titreLbl.setText(titre);
		
		this.getContentPane().add(titreLbl, BorderLayout.NORTH);
		this.getContentPane().add(panContent, BorderLayout.CENTER);
		this.getContentPane().add(panBouton, BorderLayout.SOUTH);
		this.getContentPane().add(panIcon, BorderLayout.EAST);
		
		
	}
	
	public void initComponent() {}
	
}
