package garage_bdd_test_boite_dialogue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.text.html.Option;

import garage_bdd_test_DAO.VehiculeDAO;

/**
 * Boite de dialogue modale pour la suppression d'un v�hicule
 * @author nicolas
 *
 */
public class Boite_Dial_Suppression extends Boite_dial_modale{
	private Vehicule vehicSuppr;
	private static final long serialVersionUID = 1L;

	public Boite_Dial_Suppression(JFrame parent, String ptitre, boolean modal) {
		super(parent, ptitre, modal);
		titre = "SUPPRESSION DE VEHICULE";
		VehiculeDAO vehicDAO = new VehiculeDAO(conn);
		this.vehicSuppr = vehicDAO.find();//--recup l'id du vehicule � partir de la table
	}
	
	/**
	 * Methode initialisant les diff�rents composants de la boite de dialogue
	 */
	public void initComponent() {
		
		//--nom du vehicule
		JPanel panNom = new JPanel();
		panNom.setBackground(Color.WHITE);
		panNom.setPreferredSize(new Dimension (220,60));
		panNom.setBorder(BorderFactory.createTitledBorder("Nom du v�hicule"));
		JLabel nomLabel = new JLabel(vehicSuppr.getNom());
		panNom.add(nomLabel);
		
		//--Marque
		JPanel panMarque = new JPanel();
		panMarque.setBackground(Color.WHITE);
		panMarque.setPreferredSize(new Dimension(220,60));
		panMarque.setBorder(BorderFactory.createTitledBorder("Marque du vehicule"));
		JLabel marqueLabel = new JLabel("Marque : ");
		JLabel marque = new JLabel(vehicSuppr.getMarque().getNom());
		panMarque.add(marqueLabel);
		panMarque.add(marque);
		
		//--Image du vehicule selon la marque
		String constrVehic = marque.getText();
		switch(constrVehic) {
			case("RENO") : this.icon = new JLabel(new ImageIcon("Ressource/Images/RENO.JPG")); break;
			case("TROEN") : this.icon = new JLabel(new ImageIcon("Ressource/Images/TROEN.JPG")) ; break;
			case ("PIGEOT") : this.icon = new JLabel(new ImageIcon("Ressource/Images/PIGEOT.JPG")); break;
		}
				
		//--type de moteur
		JPanel panTypeMoteur = new JPanel();
		panTypeMoteur.setBackground(Color.WHITE);
		panTypeMoteur.setPreferredSize(new Dimension(440,60));
		panTypeMoteur.setBorder(BorderFactory.createTitledBorder("Type du moteur du vehicule"));
		JRadioButton tranche1 = new JRadioButton("ESSENCE");
		JRadioButton tranche2 = new JRadioButton("DIESEL");
		JRadioButton tranche3 = new JRadioButton("HYBRIDE");
		JRadioButton tranche4 = new JRadioButton("ELECTRIQUE");
		String constrTM = vehicSuppr.getTypeMoteur().getNom();
		switch(constrTM) {
			case("ESSENCE") : tranche1.setSelected(true); break;
			case("DIESEL") : tranche2.setSelected(true) ; break;
			case ("HYBRIDE") : tranche3.setSelected(true); break;
			case("ELECTRIQUE") : tranche4.setSelected(true); break;
		}
		ButtonGroup bg = new ButtonGroup();
		bg.add(tranche1);
		bg.add(tranche2);
		bg.add(tranche3);
		bg.add(tranche4);
		panTypeMoteur.add(tranche1);
		panTypeMoteur.add(tranche2);
		panTypeMoteur.add(tranche3);
		panTypeMoteur.add(tranche4);
				
		//--cylindree du vehicule
		JPanel panCyl = new JPanel();
		panCyl.setBackground(Color.WHITE);
		panCyl.setPreferredSize(new Dimension(220,60));
		panCyl.setBorder(BorderFactory.createTitledBorder("Cylindree du vehicule"));
		JLabel cylindreLabel = new JLabel("Cylindree : ");
		JLabel cylDetail = new JLabel(vehicSuppr.getMoteur().getCylindre());
		panCyl.add(cylindreLabel);
		panCyl.add(cylindre);
		
		//--Options
		JPanel panOption = new JPanel();
		panOption.setBackground(Color.WHITE);
		panOption.setPreferredSize(new Dimension(440,60));
		panOption.setBorder(BorderFactory.createTitledBorder("Options du vehicule"));
		option1 = new JCheckBox("Toit ouvrant");
		option2 = new JCheckBox("Climatisation");
		option3 = new JCheckBox("GPS");
		option4 = new JCheckBox("Sieges chauffants");
		option5 = new JCheckBox("Barres de toit");
		List<Option> constrOpt = vehicSuppr.getOption();
		for (Option opt : constrOpt) {
			switch(opt.getNom()) {
				case("Toit ouvrant") : option1.setSelected(true); break;
				case("Climatisation") : option2.setSelected(true) ; break;
				case ("GPS") : option3.setSelected(true); break;
				case("Sieges chauffants") : option4.setSelected(true); break;
				case("Barres de toit") : option5.setSelected(true); break;
			}
		}
		panOption.add(option1);
		panOption.add(option2);
		panOption.add(option3);
		panOption.add(option4);
		panOption.add(option5);
		
		//--Prix du vehicule
		JPanel panPrix = new JPanel();
		panPrix.setBackground(Color.WHITE);
		panPrix.setPreferredSize(new Dimension (220,60));
		panPrix.setBorder(BorderFactory.createTitledBorder("Prix du vehicule"));
		JLabel prixLabel1 = new JLabel("Prix");
		JLabel prixLabel2 = new JLabel(" �");
		JLabel prix = new JLabel(vehicSuppr.getPrix());
		panPrix.add(prixLabel1);
		panPrix.add(prix);
		panPrix.add(prixLabel2);
				
		//--On ajoute les composants precedents au JPanel qui doit recevoir tout cela		
		panContent.add(panNom);
		panContent.add(panMarque);
		panContent.add(panTypeMoteur);
		panContent.add(panCyl);
		panContent.add(panOption);
		panContent.add(panPrix);
		
		//--On ajoute un listener a notre bouton OK
		this.okBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VehiculeDAO vehicSupprDAO = new VehiculeDAO(conn);
				vehicSupprDAO.delete(vehicSuppr);
				setVisible(false);
			}
		});
	}

}
