package le_pendu_test;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * On test l'affichage de la liste des scores ds le panneau Score
 * @author nicolas
 *
 */
public class Test4 extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private Joueur1 toi = new Joueur1("moi", 0);
	private Joueur1 moi = new Joueur1("moi", 1500);
	private Joueur1 nous = new Joueur1("moi", 25);
	private Joueur1 j1 = new Joueur1("aaa", 125);
	private Joueur1 j2 = new Joueur1("bbb", 28);
	private Joueur1 j3 = new Joueur1("ccc", 89);
	private Joueur1 j4 = new Joueur1("ddd", 6325);
	private Joueur1 j5 = new Joueur1("eee", 457);
	private Joueur1 j6 = new Joueur1("fff", 752);
	private Joueur1 j7 = new Joueur1("ggg", 41);
	private Joueur1 j8 = new Joueur1("hhh", 6325);
	private Joueur1 j9 = new Joueur1("iii", 4);
	private Joueur1 j10 = new Joueur1("kkk", 712);
	private Joueur1 j11 = new Joueur1("lll", 120);
	private Joueur1 j12 = new Joueur1("mmm", 7541);
	private Joueur1 j13 = new Joueur1("nnn", 478);
	private Joueur1 j14 = new Joueur1("ooo", 210);
	private Joueur1 j15 = new Joueur1("ppp", 78952);
	private Joueur1 j16 = new Joueur1("qqq", 65);
	private JoueurSerializer js;
	private Scores score = new Scores();
		
	public Test4() {
		this.setTitle("Test Score");
		this.setForeground(Color.BLACK);
		this.setBackground(Color.WHITE);
		this.setSize(new Dimension(800,650));
		this.setResizable(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		initTest();
		
		this.getContentPane().add(score);
		this.setVisible(true);
	}
		
	private void initTest() {		
		
		this.js = new JoueurSerializer();
		
		this.js.getListe().add(j1);
		this.js.getListe().add(j2);
		this.js.getListe().add(j3);
		this.js.getListe().add(j4);
		this.js.getListe().add(j5);
		this.js.getListe().add(j6);
		this.js.getListe().add(j7);
		this.js.getListe().add(j8);
		this.js.getListe().add(j9);
		this.js.getListe().add(j10);
		this.js.getListe().add(j11);
		this.js.getListe().add(j12);
		this.js.getListe().add(j13);
		this.js.getListe().add(j14);
		this.js.getListe().add(j15);
		this.js.getListe().add(j16);
	   
		
		

		js.sauvegarderFile();
		js.lireFile();
		js.checkJoueur(moi);
		js.sauvegarderFile();
		js.checkJoueur(toi);
		js.sauvegarderFile();
		js.checkJoueur(nous);
		js.sauvegarderFile();
		js.lireFile();

	}
	
	
	
	

	public static void main(String[] args) {
		Test4 t = new Test4();

	}

}
