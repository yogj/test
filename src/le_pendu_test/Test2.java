package le_pendu_test;

/**
 * On trie notre liste : on la lit, on ajoute un joueur, on la trie et on la svg
 * @author nicolas
 *
 */

public class Test2 {
	private Joueur1 toi = new Joueur1("moi", 1500);
	private Joueur1 moi = new Joueur1("moi", 0);
	private Joueur1 j1 = new Joueur1("aaa", 125);
	private Joueur1 j2 = new Joueur1("bbb", 28);
	private Joueur1 j3 = new Joueur1("ccc", 89);
	private Joueur1 j4 = new Joueur1("ddd", 6325);
	private Joueur1 j5 = new Joueur1("eee", 457);
	private Joueur1 j6 = new Joueur1("fff", 752);
	private Joueur1 j7 = new Joueur1("ggg", 41);
	private Joueur1 j8 = new Joueur1("hhh", 6325);
	private Joueur1 j9 = new Joueur1("iii", 4);
	private Joueur1 j10 = new Joueur1("kkk", 712);
	private Joueur1 j11 = new Joueur1("lll", 120);
	private Joueur1 j12 = new Joueur1("mmm", 7541);
	private Joueur1 j13 = new Joueur1("nnn", 478);
	private Joueur1 j14 = new Joueur1("ooo", 210);
	private Joueur1 j15 = new Joueur1("ppp", 78952);
	private Joueur1 j16 = new Joueur1("qqq", 65);
	private JoueurSerializer js;

	
	public Test2() {
		this.js = new JoueurSerializer();
		
		this.js.getListe().add(j1);
		this.js.getListe().add(j2);
		this.js.getListe().add(j3);
		this.js.getListe().add(j4);
		this.js.getListe().add(j5);
		this.js.getListe().add(j6);
		this.js.getListe().add(j7);
		this.js.getListe().add(j8);
		this.js.getListe().add(j9);
		this.js.getListe().add(j10);
		this.js.getListe().add(j11);
		this.js.getListe().add(j12);
		this.js.getListe().add(j13);
		this.js.getListe().add(j14);
		this.js.getListe().add(j15);
		this.js.getListe().add(j16);
	   
		
		

		js.sauvegarderFile();
		js.lireFile();
		js.getListe().add(moi);
		js.sauvegarderFile();
		js.getListe().add(toi);
		js.sauvegarderFile();
	}
	
	public static void main(String[] args) {
		Test2 t = new Test2();
		

	}
}
