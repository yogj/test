package le_pendu_test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;



public class Scores extends JPanel{
	/**
	 * variables d'instance
	 */
	private static final long serialVersionUID = 1L;
	private JLabel[] listEtiquette = new JLabel[10];
	private CopyOnWriteArrayList<Joueur1> listJoueur;
	private JLabel img = new JLabel(new ImageIcon("Ressources/Images/imgPendu.JPG")),
				   titre = new JLabel();
	private JPanel panLabel = new JPanel();
	private String nom = "Scores";
	private JoueurSerializer gestionFile;

	
	public Scores() {
		//--on param le rangement des comp et la couleur de fond
		this.setLayout(new BorderLayout());
		this.setBackground(Color.WHITE);
		
		//--Etiquette et panneau titre
		Font policeTitre = new Font("Arial", Font.BOLD, 35);
		titre.setFont(policeTitre);
		titre.setPreferredSize(new Dimension (500,100));
		titre.setHorizontalAlignment(JLabel.CENTER);
		titre.setForeground(Color.BLACK);
		titre.setText("PANNEAU DES SCORES");
		this.add(titre, BorderLayout.NORTH);
		
		//--on init la liste en recuperant la liste de notre fichier
		this.gestionFile = new JoueurSerializer();
		this.gestionFile.lireFile();
		this.listJoueur = this.gestionFile.getListe();
		System.out.println("liste de joueur sur page score \n"+this.gestionFile.toString());//Controle
		System.out.println("liste de page score " +this.listJoueur);
		//--On recup les differents joueurs de la listJoueur
		//for(Joueur1 j : this.listJoueur) {
		Font police = new Font("Arial", Font.BOLD, 18);
		panLabel.setLayout(new BoxLayout(panLabel, BoxLayout.PAGE_AXIS));
		//--On affiche dans 10 etiquettes le nom du joueur et son score
		for(int i = 0; i <10; i++) {
			listEtiquette[i] = new JLabel();
			listEtiquette[i].setFont(police);
			listEtiquette[i].setPreferredSize(new Dimension(400,100));
			listEtiquette[i].setForeground(Color.BLACK);
			listEtiquette[i].setText(+i + 1+". "+this.listJoueur.get(i).getNom()+" .................. "+this.listJoueur.get(i).getScore());
			System.out.println("etiquettes "+i+" joueur "+this.listJoueur.get(i).getNom()+" - score "+this.listJoueur.get(i).getScore()+"\n");//Controle
			panLabel.add(listEtiquette[i]);
		}
		System.out.println("liste etq : " +this.listEtiquette);

		this.add(panLabel, BorderLayout.WEST);
		
		//--Etiquette affichant l'image
		this.add(img, BorderLayout.EAST);
	}
	
	public String getNom() {
		return this.nom;
	}

}
