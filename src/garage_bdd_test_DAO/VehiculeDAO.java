package garage_bdd_test_DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import projet53_pattern_DAO.Professeurs;
import projet53_pattern_DAO_implement.MatieresDAO;
/**
 * Classe d�finissant l'objet VehiculeDAO
 * @author nicolas
 *
 */
public class VehiculeDAO extends DAO_abstr<Vehicule> {
	/**
	 * Constructeur avec comme parametre la connection etablie par le Connecteur
	 * @param pConn
	 */
	public VehiculeDAO(Connection pConn) {
		super(pConn);
	}

	/**
	 * M�thode de cr�ation d'un objet Vehicule dans les tables vehicule et vehicule_option
	 */
	public boolean create(Vehicule obj) {
		// On d�marre notre transction
		this.conn.setAutoCommit(false);
		// Nous allons r�cup�rer le prochain ID
		ResultSet nextID = this.conn.prepareStatement("CALL NEXT VALUE FOR seq_vehicule_id").executeQuery();
		 if (nextID.next()) {
		 	 int ID = nextID.getInt(1);
		 	 if(this.find(ID) == null) {	
		 		 try {
		 			 //--On creee le vehicule ds la table vehicule
		 			 ResultSet resultat1 = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
		 					 "INSERT INTO Vehicule VALUES ("+obj.getMarque()+", "+obj.getMoteur()+", "+obj.getPrix()+", '"+obj.getNom()+"', null)");
		 			 //--Pour chaque option du v�hicule, on l'inscrit ds la table vehicule_option
		 			 for (Option opt : obj.getOption()) {
		 				 ResultSet resultat2 = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
		 					 "INSERT INTO Vehicule_option VALUES("+obj.getId() +","+opt+")");
		 			 }
		 		 }catch (SQLException e) {
		 			 e.printStackTrace();
		 		 }
		 		 return true;	
		 	 }
		 	 return false;
		 }
	}

	/**
	 *M�thode de suppression d'un objet Vehicule dans les tables vehicule et vehicule_option 
	 */
	public boolean delete(Vehicule obj) {
		if(!obj == null) {
			try {
				//--On supprime les options du vehicules dans la table vehicule_option
				ResultSet resultat1 = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
						"DELETE FROM Vehicule_option WHERE id_vehicule = "+obj.getId());
				//--On supprime le vehicule ds la table vehicule
				ResultSet resultat2 = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
						"DELETE FROM Vehicule WHERE id = "+obj.getId());						
			}catch (SQLException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	/**
	 * M�thode de modification d'un objet Vehicule dans les tables vehicule et vehicule_option
	 */
	public boolean update(Vehicule obj) {
		if (!obj == null) {
			if (!this.find(obj.getId()==null)) {
				try {
					//--On modif le vehicule ds la table vehicule
					ResultSet resultat1 = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
							"UPDATE Vehicule SET nom = '"+obj.getNom()+"', marque = '"+obj.getMarque()+"', moteur = '"+obj.getMoteur()+"',"
									+ "prix = '"+obj.getPrix()+"' WHERE id = "+obj.getId());
					//--On modif les options du vehicule ds la table vehicule_option
					ResultSet resultat2 = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
							"UPDATE Vehicule_option SET id_option = '"+obj.getOption().getId()+"'WHERE id_vehicule = "+obj.getId());
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * M�thode de recherche d'un objet Vehicule dans la tables vehicule et de ses options dans la table vehicule_option
	 */
	public Vehicule find(int id) {
		Vehicule voiture = new Vehicule();
		Marque marque = new Marque();
		Moteur moteur = new Moteur();
		Option opt = new Option();
		List<Option> lopt = new List<Option>();
		
		try {
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM Vehicule INNER JOIN vehicule_option ON id_vehicule = id AND id = "+id+" LEFT JOIN option ON id_option = option.id");
			if(resultat.first()) {
				//--On recup la marque
				MarqueDAO marqueDAO = new MarqueDAO(this.conn);
				marque = marqueDAO.find(resultat.getInt("MARQUE"));
				
				//--On recupere le moteur
				MoteurDAO moteurDAO = new MoteurDAO(this.conn);
				moteur = moteurDAO.find(resultat.getInt("MOTEUR"));
				 
				//--On recup les options				
				resultat.beforeFirst();
				OptionDAO optionDAO = new OptionDAO(this.conn);
				while(resultat.next()) {
					opt = optionDAO.find(resultat.getInt("id_option"));
					lopt.addOption(opt);
				}
				//--On cree l'objet Vehicule
				voiture = new Vehicule(id, resultat.getString("nom"), marque, moteur, lopt, resultat.getDouble("prix"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return voiture;
	}
	/**
	 * Methode retournant l'Id de l'objet Vehicule 
	 */
	public Vehicule getIdentifiant(String pNom) {
		Vehicule vehic = new Vehicule();
		Marque marque = new Marque();
		Moteur moteur = new Moteur();
		Option opt = new Option();
		List<Option> lopt = new List<Option>();
		int idVoit = 0;
		try {
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT id FROM Vehicule WHERE nom = '"+pNom.toUpperCase()+"'");
			if(resultat.first()) {
				idVoit = (Integer)resultat.getInt("Id");
				//--On recup la marque
				MarqueDAO marqueDAO = new MarqueDAO(this.conn);
				marque = marqueDAO.find(resultat.getInt("MARQUE"));
				
				//--On recupere le moteur
				MoteurDAO moteurDAO = new MoteurDAO(this.conn);
				moteur = moteurDAO.find(resultat.getInt("MOTEUR"));
							
				//--On recup les options				
				resultat.beforeFirst();
				OptionDAO optionDAO = new OptionDAO(this.conn);
				while(resultat.next()) {
					opt = optionDAO.find(resultat.getInt("id_option"));
					lopt.addOption(opt);		
				}
				vehic = new Vehicule(idVoit, pNom, marque, moteur, lopt, resultat.getDouble("PRIX"));
			}
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return vehic;
	}
	

}
