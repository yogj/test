package garage_bdd_test_DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Classe d�finissant l'objet MarqueDAO
 * @author nicolas
 *
 */
public class MarqueDAO extends DAO_abstr<Marque> {
	/**
	 * Constructeur avec comme parametre la connection etablie par le Connecteur
	 * @param pConn
	 */
	public MarqueDAO(Connection pConn) {
		super(pConn);
	}

	/**
	 * M�thode de cr�ation d'un objet Marque dans la table Marque
	 */
	public boolean create(Marque obj) {
		int id = obj.getId();
		if(this.find(id) == null) {	
			try {
				ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
					"INSERT INTO Marque VALUES ("+obj.getId()+", '"+obj.getNom().toUpperCase()+"')");
				}catch (SQLException e) {
					e.printStackTrace();
				}
			return true;	
			}
		return false;
	}

	/**
	 * M�thode de suppression d'un objet Marque de la table Marque
	 */
	public boolean delete(Marque obj) {
		if(!obj == null) {
			try {
				ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
						"DELETE FROM Marque WHERE id = "+getIdentifiant(obj.getNom().toUpperCase()));
			}catch (SQLException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	/**
	 * M�thode de modification d'un objet Marque dans la table Marque
	 */
	public boolean update(Marque obj) {
		if (!obj == null) {
			if (!this.find(obj.getId()==null)) {
				try {
					ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
							"UPDATE Marque SET nom = '"+obj.getNom().toUpperCase()+"' WHERE id = "+getIdentifiant(obj.getNom().toUpperCase()));
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * M�thode de recherche d'un objet Marque dans la table Marque
	 */
	public Marque find(int id) {
		Marque marque = new Marque();
		try {
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM Marque WHERE id = "+id);
			if (resultat.first())
				marque = new Marque(id, resultat.getString("nom"));
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return marque;
	}
	/**
	 * Methode retournant l'Id de l'objet Marque 
	 */
	public Marque getIdentifiant(String pNom) {
		Marque marque = new Marque();
		int idMarque = 0;
		try {
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT id FROM Marque WHERE nom = '"+pNom.toUpperCase()+"'");
			if(resultat.first()) {
				idMarque = (Integer)resultat.getInt("Id");
				marque = new Marque(idMarque, pNom.toUpperCase());
			}
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return marque;
	}
}
