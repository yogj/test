package garage_bdd_test_DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Classe d�finissant l'objet MoteurDAO
 * @author nicolas
 *
 */
public class MoteurDAO extends DAO_abstr<Moteur>{
	/**
	 * Constructeur avec comme parametre la connection etablie par le Connecteur
	 * @param pConn
	 */
	public MoteurDAO(Connection pConn) {
		super(pConn);
	}

	/**
	 * M�thode de cr�ation d'un objet Moteur dans la table Moteur
	 */
	public boolean create(Moteur obj) {
		int id = obj.getId();
		if(this.find(id) == null) {	
			try {
				ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
					"INSERT INTO Moteur VALUES ("+obj.getId()+", "+obj.getType()+",'"+ obj.getCylindre().toLowerCase()+"'," +obj.getPrix()+")");
				}catch (SQLException e) {
					e.printStackTrace();
				}
			return true;	
			}
		return false;
	}

	/**
	 * M�thode de suppression d'un objet Moteur de la table Moteur
	 */
	public boolean delete(Moteur obj) {
		if(!obj == null) {
			try {
				ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
						"DELETE FROM Moteur WHERE id = "+getIdentifiant(obj.getId().toLowerCase()));
			}catch (SQLException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	/**
	 * M�thode de modifcation d'un objet Moteur dans la table Moteur
	 */
	public boolean update(Moteur obj) {
		if (!obj == null) {
			if (!this.find(obj.getId()==null)) {
				try {
					ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
							"UPDATE Moteur SET cylindre = '"+obj.getCylindre().toLowerCase())+"', moteur = "+obj.getType()+", prix = "+obj.getPrix()+
							" WHERE id = "+getIdentifiant(obj.getCylindre().toLowerCase());
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * M�thode de recherche d'un objet Moteur dans la table Moteur
	 */
	public Moteur find(int id) {
		Moteur moteur  = new Moteur();
		try {
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM Moteur WHERE id = "+id);
			if (resultat.first())
				moteur = new Moteur(id, resultat.getString("cylindre"), resultat.getInt("moteur"), resultat.getDouble("prix"));
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return moteur;
	}
	/**
	 * Methode retournant l'Id de l'objet Moteur
	 */
	public Moteur getIdentifiant(String pCylindre) {
		Moteur moteur = new Moteur();
		int idMoteur = 0;
		try {
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT id FROM Moteur WHERE cylindre = '"+pCylindre.toLowerCase()+"'");
			if(resultat.first()) {
				idMoteur = (Integer)resultat.getInt("Id");
				moteur = new Moteur(idMoteur, pCylindre.toLowerCase(), resultat.getDouble("prix"));
			}
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return moteur;
	}

}
