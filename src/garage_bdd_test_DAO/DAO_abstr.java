package garage_bdd_test_DAO;

import garage_bdd_test_DAO.Connecteur;

import java.sql.Connection;
/**
 * Classe Abstraite dont herite nos objetsDAO
 * @author nicolas
 *
 * @param <T>
 */
public abstract class DAO_abstr<T> {
	protected Connection conn = null;
	/**
	 * Constructeur vaec co parametre la connexion etablie par le Connecteur
	 * @param pConn
	 */
	public DAO_abstr(Connection pConn){
		this.conn = pConn;
	}
	/**
	 * Methode abstraite de creation d'un objet de type T dans la table T
	 * @param obj
	 * @return
	 */
	public abstract boolean create (T obj);
	/**
	 *  Methode abstraite de suppression d'un objet de type T dans la table T
	 * @param obj
	 * @return
	 */
	public abstract boolean delete (T obj);
	/**
	 *  Methode abstraite de modification d'un objet de type T dans la table T
	 * @param obj
	 * @return
	 */
	public abstract boolean update (T obj);
	/**
	 *  Methode abstraite de recherche d'un objet de type T
	 * @param id
	 * @return
	 */
	public abstract T find(int id);
	/**
	 * Methode abstraite de recherche d'un objet de type T
	 * @param nom
	 * @return
	 */
	public abstract T find(String nom);
}
