package garage_bdd_test_DAO;

import java.sql.Connection;
/**
 * Classe qui construit les objetsDAO
 * @author nicolas
 *
 */
public class DAO_Factory {
	protected static final Connection conn = Connecteur.getInstance();
	
	/**
	 * Methode d'instanciation d'un objet VehiculeDAO
	 * @return VehiculeDAO
	 */
	public static DAO_abstr<Vehicule> getVehiculeDAO() {
		return new VehiculeDAO(conn);
	}
	/**
	 * Methode d'instanciation d'un objet OptionDAO
	 * @return OptionDAO
	 */
	public static DAO_abstr<Option> getOptionDAO(){
		return new OptionDAO(conn);
	}
	/**
	 * Methode d'instanciation d'un objet MoteurDAO
	 * @return MoteurDAO
	 */
	public static DAO_abstr<Moteur> getMoteurDAO(){
		return new MoteurDAO(conn);
	}
	/**
	 * Methode d'instanciation d'un objet MarqueDAO
	 * @return MarqueDAO
	 */
	public static DAO_abstr<Marque> getMarqueDAO(){
		return new MarqueDAO(conn);
	}
	/**
	 * Methode d'instanciation d'un objet TypeMoteurDAO
	 * @return TypeMoteurDAO
	 */
	public static DAO_abstr<TypeMoteur> getTypeMoteur(){
		return new TypeMoteurDAO(conn);
	}
}
