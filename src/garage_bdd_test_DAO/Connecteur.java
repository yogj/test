package garage_bdd_test_DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * Classe permettant la connexion avec la bdd
 * @author nicolas
 *
 */
public class Connecteur {
	// URL de connexion
		private String url = "jdbc:hsqldb:file:hsqldb/database/VEHICULE";
		// Nom du user
		private String user = "SA";
		// Mot de passe de l'utilisateur
		private String passwd = "";
		// Objet Connection
		private static Connection connect;
		private static Connecteur instance = new Connecteur();

		/**
		 * Constructeur priv�
		 */
		private Connecteur() {
			try {
				connect = DriverManager.getConnection(url, user, passwd);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		/**
		 * M�thode d'acc�s au singleton
		 * @return
		 */
		public static Connection getInstance() {
			if (connect == null)
				instance = new Connecteur();

			return connect;
		}
}
