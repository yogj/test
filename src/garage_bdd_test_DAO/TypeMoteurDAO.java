package garage_bdd_test_DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Classe d�finissant l'objet TypeMoteurDAO
 * @author nicolas
 *
 */
public class TypeMoteurDAO extends DAO_abstr<TypeMoteur> {
	/**
	 * Constructeur avec comme parametre la connection etablie par le Connecteur
	 * @param pConn
	 */
	public TypeMoteurDAO(Connection pConn) {
		super(pConn);
	}

	/**
	 * M�thode de cr�ation d'un objet TypeMoteur dans la table type_moteur 
	 */
	public boolean create(TypeMoteur obj) {
		int id = obj.getId();
		if(this.find(id) == null) {	
			try {
				ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
					"INSERT INTO Type_moteur VALUES ("+obj.getId()+", '"+obj.getNom().toUpperCase()+"')");
				}catch (SQLException e) {
					e.printStackTrace();
				}
			return true;	
			}
		return false;
	}

	/**
	 * M�thode de suppression d'un objet TypeMoteur de la table type_moteur 
	 */
	public boolean delete(TypeMoteur obj) {
		if(!obj == null) {
			try {
				ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
						"DELETE FROM Type_moteur WHERE id = "+getIdentifiant(obj.getNom().toUpperCase()));
			}catch (SQLException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	/**
	 * M�thode de modification d'un objet TypeMoteur dans la table type_moteur 
	 */
	public boolean update(TypeMoteur obj) {
		if (!obj == null) {
			if (!this.find(obj.getId()==null)) {
				try {
					ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
							"UPDATE Type_moteur SET description = '"+obj.getNom().toUpperCase()+"' WHERE id = "+getIdentifiant(obj.getNom()));
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return true;
		}	
		return false;
	}

	/**
	 * M�thode de recherche d'un objet TypeMoteur dans la table type_moteur 
	 */
	public TypeMoteur find(int id) {
		TypeMoteur type = new TypeMoteur();
		try {
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM Type_moteur WHERE id = "+id);
			if (resultat.first())
				type = new TypeMoteur(id, resultat.getString("description").toUpperCase());
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return type;
	}
	/**
	 * Methode retournant l'Id de l'objet TypeMoteur 
	 */
	public TypeMoteur getIdentifiant(String pDescription) {
		TypeMoteur tm = new TypeMoteur();
		int idTM = 0;
		try {
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT id FROM Type_moteur WHERE DESCRIPTION = '"+pDescription.toUpperCase()+"'");
			if(resultat.first()) {
				idTM = (Integer)resultat.getInt("Id");
				tm = new TypeMoteur(idTM, pDescription.toUpperCase());
			}
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return tm;
	}

}
