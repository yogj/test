package garage_bdd_test_DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Classe d�finissant l'objet OptionDAO
 * @author nicolas
 *
 */
public class OptionDAO extends DAO_abstr<Option> {
	/**
	 * Constructeur avec comme parametre la connection etablie par le Connecteur
	 * @param pConn
	 */
	public OptionDAO(Connection pConn) {
		super(pConn);
	}
	
	/**
	 *M�thode de cr�ation d'un objet Option dans la table option 
	 */
	public boolean create(Option obj) {
		int id = obj.getId();
		if(this.find(id) == null) {	
			try {
				ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
					"INSERT INTO Option VALUES ("+obj.getId()+", '"+obj.getNom().toLowerCase()+"',"+obj.getPrix()+")");
				}catch (SQLException e) {
					e.printStackTrace();
				}
			return true;	
			}
		return false;
	}

	/**
	 * M�thode de suppression d'un objet Option de la table option 
	 */
	public boolean delete(Option obj) {
		if(!obj == null) {
			try {
				ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
						"DELETE FROM Option WHERE id = "+getIdentifiant(obj.getNom().toLowerCase()));
			}catch (SQLException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	/**
	 * M�thode de modification d'un objet Option dans la table option 
	 */
	public boolean update(Option obj) {
		if (!obj == null) {
			if (!this.find(obj.getId()==null)) {
				try {
					ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(
							"UPDATE Option SET description = '"+obj.getNom().toLowerCase()+"', prix = "+obj.getPrix()+" WHERE id = "+getIdentifiant(obj.getNom().toLowerCase()));
				}catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * M�thode de recherche d'un objet Option dans la table option 
	 */
	public Option find(int id) {
		Option option = new Option();
		try {
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM Option WHERE id = "+id);
			if (resultat.first())
				option = new Option(id, resultat.getString("description"), resultat.getDouble("prix"));
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return option;
	}
	/**
	 * Methode retournant l'Id de l'objet Option 
	 */
	public Option getIdentifiant(String pDescription) {
		Option opt = new Option();
		int idOpt = 0;
		try {
			ResultSet resultat = this.conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT id FROM Option WHERE DESCRIPTION = '"+pDescription.toLowerCase()+"'");
			if(resultat.first()) {
				idOpt = (Integer)resultat.getInt("Id");
				opt = new Option(idOpt, pDescription.toLowerCase());
			}
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return opt;
	}
}
